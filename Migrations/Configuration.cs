namespace AssignmentMVC.Migrations
{
    using AssignmentMVC.HashValue;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AssignmentMVC.BAL.BLL>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AssignmentMVC.BAL.BLL context)
        {
            List<Contact> defaultContacts = new List<Contact>();

            defaultContacts.Add(new Contact() { ContactType = "Others" });
            defaultContacts.Add(new Contact() { ContactType = "Mobile" });
            defaultContacts.Add(new Contact() { ContactType = "Home" });
            defaultContacts.Add(new Contact() { ContactType = "Office" });
            defaultContacts.Add(new Contact() { ContactType = "fax" });
            defaultContacts.Add(new Contact() { ContactType = "Landline" });
            defaultContacts.Add(new Contact() { ContactType = "Alternate Mobile" });
            context.Contacts.AddRange(defaultContacts);

            List<Module> defaultModules = new List<Module>();
            defaultModules.Add(new Module() { ModuleName = "Admin" });
            defaultModules.Add(new Module() { ModuleName = "User" });

            context.Modules.AddRange(defaultModules);

            var user = new User()
            {
                Firstname = "admin",
                Lastname = "admin",
                Email = "admin@admin.com",
                Password = HashValue.GetHashCode("admin"),
                Address = "admin"
            };

            context.Users.Add(user);


            var access = new AccessLayer()
            {
                User = user,
                Module = defaultModules.Single(s => s.ModuleName == "Admin")
            };

            context.AccessLayers.Add(access);


            var image = new Image()
            {
                User=user,
                Title="admin",
                ImageURL=@"\Images\default.png"
            };
            context.Images.Add(image);

        }
    }
}
