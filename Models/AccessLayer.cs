﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssignmentMVC.Models
{
    [Table("AccessLayers", Schema = "Registration")]
    public class AccessLayer
    {
        [Key]
        public int AccessId { get; set; }

        [ForeignKey("Module")]
        public int ModuleId { get; set; }
        public Module Module { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }

    }
}