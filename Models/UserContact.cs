﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssignmentMVC.Models
{
    [Table("UserContacts",Schema ="Registration")]
    public class UserContact
    {
        [Key]
        public int UserContactId { get; set; }

        [MaxLength(12)]
        public string  ContactNumber{ get; set; }



        [ForeignKey("User")]
        public int UserId { get; set; }
        public User User { get; set; }

        [ForeignKey("Contact")]
        public int ContactId { get; set; }
        public Contact Contact { get; set; }
    }
}

