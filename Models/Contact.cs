﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AssignmentMVC.Models
{
    [Table("Contacts", Schema = "Registration")]
    public class Contact
    {
        [Key]
        public int ContactId { get; set; }
        [MaxLength(30)]
        public string ContactType { get; set; }
    }
}