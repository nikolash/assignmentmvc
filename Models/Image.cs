﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AssignmentMVC.Models
{
    [Table("Images", Schema = "Registration")]
    public class Image
    {
        [Key]
        public int ImageId { get; set; }


        [MaxLength(25)]
        public string Title { get; set; }
        public string ImageURL { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public User User { get; set; }
    }
}