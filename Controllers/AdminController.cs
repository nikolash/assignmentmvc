﻿using AssignmentMVC.BAL;
using AssignmentMVC.BusinessClass;
using AssignmentMVC.ViewModel;
using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace AssignmentMVC.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {

        /// <summary>
        /// Dashboard for Admin after login
        /// </summary>
        DAL LogicObject = new DAL();
        public ActionResult Index()
        {
            var localEmail = UserCookie.GetCookie(Request);
            if (localEmail != string.Empty)
            {
                var obj = LogicObject.ReadData(localEmail);
                return View(obj);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }



        /// <summary>
        /// Get all Users in the Database to work-with.
        /// </summary>
        /// <returns>List of USERS</returns>
        public ActionResult Users()
        {
            var localEmail = UserCookie.GetCookie(Request);
            if (localEmail != string.Empty)
            {
                BLL reference = new BLL();
                var user = reference.Users.Where(m => m.Email != localEmail).ToList();
                return View(user);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }


        /// <summary>
        /// Gives details of the user
        /// </summary>
        /// <param name="id"></param>
        /// <returns>User Details</returns>
        [HandleError(ExceptionType = typeof(SqlException), View = "Error")]
        public ActionResult Details(int id)
        {
            var localEmail = UserCookie.GetCookie(Request);
            if (localEmail != string.Empty)
            {
                BLL reference = new BLL();
                var dataObject = reference.Users.Where(m => m.UserId == id).SingleOrDefault();
                var image = reference.Images.Where(m => m.UserId == id).SingleOrDefault();
                var contactObject = reference.UserContacts.Where(m => m.UserId == id).Include(m => m.Contact).ToList();
                var access = reference.AccessLayers.Where(m => m.UserId == id).Include(m => m.Module).ToList();
                var displayData = new UserInformation()
                {
                    User = dataObject,
                    Image = image,
                    UserContacts = contactObject,
                    Accesses = access
                };
                return View(displayData);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }


        /// <summary>
        /// Promote a User to Admin or User Type.
        /// </summary>
        /// <param name="promote"></param>
        /// <returns>JSON</returns>
        [HandleError(ExceptionType = typeof(SqlException), View = "Error")]
        [ValidateAntiForgeryToken]
        public ActionResult Promote(Promote promote)
        {
            var localEmail = UserCookie.GetCookie(Request);
            if (localEmail != string.Empty)
            {
                BLL reference = new BLL();
                var localObject = reference.AccessLayers.Where(m => m.UserId == promote.UserId).SingleOrDefault();
                localObject.ModuleId = promote.PromoteId;
                reference.SaveChanges();
                return Json(new { Result = "Success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }

        }


        /// <summary>
        /// Delete a User by Admin by the ID of the USER
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HandleError(ExceptionType = typeof(SqlException), View = "Error")]
        [HttpGet]
        public ActionResult Delete(int UserId)
        {
            

            var localEmial = UserCookie.GetCookie(Request);
            if (localEmial != string.Empty)
            {

                BLL reference = new BLL();
                var localObject = reference.Users.Where(m => m.UserId == UserId).SingleOrDefault();
                reference.Users.Remove(localObject);
                reference.SaveChanges();
                return RedirectToAction("Users", "Admin");
            }

            else
            {
                return RedirectToAction("Login", "Login");
            }
        }




        /// <summary>
        /// Get Users information and returns a JSON when the Admin fetch data with the ID of USER
        /// </summary>
        /// <param name="id"></param>
        /// <returns>JSON</returns>
        [HandleError(ExceptionType = typeof(SqlException), View = "Error")]
        [HttpPost]
        public ActionResult GetUserInfo(int id)
        {
           
            BLL reference = new BLL();
            var data = reference.Users.Where(m => m.UserId == id).SingleOrDefault();
            var editObject = new EditModel()
            {
                UserId = data.UserId,
                Firstname = data.Firstname,
                Lastname = data.Lastname,
                Address = data.Address
            };
            return Json(editObject);
        }
    }
}