﻿using AssignmentMVC.BAL;
using AssignmentMVC.ViewModel;
using Elmah;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AssignmentMVC.Controllers
{
    [Authorize]
    public class LoginController : Controller
    {
        /// <summary>
        /// Get for LoginView
        /// </summary>
        /// <returns>View</returns>
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }




        /// <summary>
        /// Authenticate the USER
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns>Dashboard View</returns>
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel loginModel)
        {
            DAL logicObject = new DAL();
            int userId = logicObject.LogIn(loginModel);
            if (userId != 0)
            {
                FormsAuthentication.SetAuthCookie(loginModel.Email, false);
                return RedirectToAction("DashBoard", "User");
            }
            else
            {
                ViewBag.ErrorMessage = "Invalid Login Details.";
                return View("Login");
            }
        }
    }
}