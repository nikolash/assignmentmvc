﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace AssignmentMVC.BusinessClass
{
    public static class UserCookie
    {
        public static string GetCookie(HttpRequestBase request)
        {
            return FormsAuthentication.Decrypt(request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
        }
    }
}