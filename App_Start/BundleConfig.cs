﻿using System.Web.Optimization;

namespace AssignmentMVC.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            ScriptBundle scriptBndl = new ScriptBundle("~/Bundles/JS");
            scriptBndl.Include(
                                "~/Scripts/modernizr-2.6.2.js",
                                "~/JQuery/dist/jquery.validate.js",
                                "~/JQuery/lib/jquery-1.11.1.js",
                                "~/JQuery/JavaScript.js",
                                "~/Scripts/jquery-{version}.js",
                                "~/Scripts/jquery.validate*"
                              );

            bundles.Add(scriptBndl);

            StyleBundle stylebndl = new StyleBundle("~/Bundles/CSS");
            stylebndl.Include(
                                "~/Content/bootstrap.css",
                                "~/Content/Site.css"
                );
            bundles.Add(stylebndl);
        }
    }
}