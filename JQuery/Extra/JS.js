﻿$("#LoginForm").validate({
    rules: {
        Email: {
            required: true,
            email: true
        },
        Password: {
            required: true,
            minlength: 8,
            passwordPattern: true
        }
    },
    messages: {
        Email: {
            required: "This field is required !",
        },
        Password: {
            required: "This field is required !"
        }
    }
});




$("#RegistrationForm").validate({

    rules: {
        Firstname: {
            required: true,
            lettersOnly: true,
            maxlength: 25
        },
        Lastname: {
            required: true,
            lettersOnly: true,
            maxlength: 25
        },
        Email: {
            required: true,
            email: true
        },
        Address: {
            required: true
        },
        Password: {
            required: true,
            minlength: 8,
            passwordPattern: true
        },
        ConfirmPassword: {
            required: true,
            equalTo: "#Password"
        }
    },
    messages: {
        firstname: {
            required: "This field is required !"
        },
        Lastname: {
            required: "This field is required !"
        },
        Email: {
            required: "This field is required !",
        },
        Address: {
            required: "This field is required !"
        },
        Password: {
            required: "This field is required !"
        },
        ConfirmPassword: {
            required: "This field is required !",
            equalTo: "Password does't match !"
        }
    }
});
$.validator.addMethod("lettersOnly", function (value, element) {
    return /^[a-z ]+$/i.test(value);
}, "Only alphabets are allowed for this field !"
);
$.validator.addMethod("passwordPattern", function (value, element) {
    return /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/.test(value);
}, "This field will take minimum 8 charectres(Lowercase,Uppercase,Numeric and Special charecters)!"
);




