﻿using AssignmentMVC.Models;
using System.Collections.Generic;

namespace AssignmentMVC.ViewModel
{
    public class UserInformation
    {
        public User User { get; set; }
        public Image Image { get; set; }
        public List<UserContact> UserContacts { get; set; }
        public List<AccessLayer> Accesses { get; set; }
    }
}