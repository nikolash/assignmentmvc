﻿using System.ComponentModel.DataAnnotations;

namespace AssignmentMVC.ViewModel
{
    public class EditModel
    {
        public int UserId { get; set; }




        [Required(ErrorMessage = "This field is required.")]
        [MaxLength(25, ErrorMessage = "Maximum 25 charecters allowed for this field.")]
        [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Only alphabets are allowed for this field.")]
        public string Firstname { get; set; }



        [Required(ErrorMessage = "This field is required.")]
        [MaxLength(25, ErrorMessage = "Maximum 25 charecters allowed for this field.")]
        [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Only alphabets are allowed for this field.")]
        public string Lastname { get; set; }


        [Required(ErrorMessage = "This field is required.")]
        public string Address { get; set; }
    }
}