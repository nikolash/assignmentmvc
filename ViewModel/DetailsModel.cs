﻿using AssignmentMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssignmentMVC.ViewModel
{
    public class DetailsModel
    {
        public int UserId{ get; set; }
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }
        public Image Image { get; set; }
        public List<UserContact> UserContact { get; set; }
    }
}