﻿using AssignmentMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.IO;
namespace AssignmentMVC.ViewModel
{
    public class RegistrationModel
    {
        [Required(ErrorMessage ="This field is required.")]
        [MaxLength(25,ErrorMessage ="Maximum 25 charecters allowed for this field.")]
        [RegularExpression("^[a-zA-Z ]+$",ErrorMessage ="Only alphabets are allowed for this field.")]
        public string Firstname { get; set; }




        [Required(ErrorMessage = "This field is required.")]
        [MaxLength(25, ErrorMessage = "Maximum 25 charecters allowed for this field.")]
        [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Only alphabets are allowed for this field.")]
        public string Lastname { get; set; }





        [Required(ErrorMessage = "This field is required.")]
        [EmailAddress(ErrorMessage ="This Email address is invalid.")]
        public string Email { get; set; }




        [Required(ErrorMessage = "This field is required.")]
        [MaxLength(50, ErrorMessage = "Maximum 50 charecters allowed for this field.")]
        [RegularExpression("^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$",ErrorMessage = "This field will take minimum 8 charectres(Lowercase,Uppercase,Numeric and Special charecters)!")]
        public string Password { get; set; }



        [Required(ErrorMessage = "This field is required.")]
        [Compare("Password",ErrorMessage ="Confirm password is not matching with password.")]
        public string ConfirmPassword { get; set; }




        [Required(ErrorMessage = "This field is required.")]
        public string Address { get; set; }

        public HttpPostedFileBase ImageURL { get; set; }


        public Contact[] Contact { get; set; }
        public string[] ContactNumber { get; set; }

    }
    public enum Contact
    {
        ContactType=1,
        Mobile,
        Home,
        Office,
        Fax,
        AlternateMobile,
        Landline
    }
}






/*var elementGenerator = '<div><input id="ContactNumber" name="ContactNumber" type="text" value=""></div>';
$("#AddContact").click(function () {
    console.log("Text");
    $("#CloneItem").clone().append(elementGenerator).insertBefore("#AppendItem");
});
*/

//from table1 in Reference.UserContacts join table2 in Reference.Contacts on table1.ContactId equals table2.ContactId where table2.ContactId==table1.ContactId select new {ContactNumber=table1.ContactNumber,ContactType=table2.ContactType }