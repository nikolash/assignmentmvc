﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AssignmentMVC.ViewModel
{
    public class ChangePasswordModel
    {
        public string Email { get; set; }

        public string Password { get; set; }


        [Required(ErrorMessage = "This field is required.")]
        [MaxLength(50, ErrorMessage = "Maximum 50 charecters allowed for this field.")]
        public string NewPassword { get; set; }



        [Required(ErrorMessage = "This field is required.")]
        [Compare("Password", ErrorMessage = "Password is not matching.")]
        public string ConfirmPassword { get; set; }

    }
}