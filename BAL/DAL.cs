﻿using System.Linq;
using System.Web;
using AssignmentMVC.ViewModel;
using AssignmentMVC.Models;
using System.Data.Entity;
using System.IO;
using System.Collections.Generic;
using System;
using System.Web.Mvc;

namespace AssignmentMVC.BAL
{

    //This is the Class for All "CRUD" operations with the database.
    public class DAL
    {
        BLL Reference = new BLL();


        /// <summary>
        /// Register User details
        /// </summary>
        /// <param name="values"></param>
        /// <returns>Email of USER</returns>

        public string Create(RegistrationModel values)
        {
            //User Table Details

            var userObject = new User()
            {
                Firstname = values.Firstname,
                Lastname = values.Lastname,
                Email = values.Email,
                Address = values.Address,
                Password = HashValue.HashValue.GetHashCode(values.Password),
            };
            Reference.Users.Add(userObject);
            Reference.SaveChanges();

            var accessLevel = new AccessLayer()
            {
                UserId = userObject.UserId,
                ModuleId = 2
            };
            Reference.AccessLayers.Add(accessLevel);
            Reference.SaveChanges();


            //ContactList For User Table 

            List<UserContact> localObject = new List<UserContact>();

            for (int flag = 0; flag < values.Contact.Count(); flag++)
            {
                if (values.ContactNumber[flag] != string.Empty)
                {

                    var contactList = new UserContact()
                    {
                        ContactNumber = values.ContactNumber[flag],
                        ContactId = (int)values.Contact[flag],
                        UserId = userObject.UserId
                    };
                    localObject.Add(contactList);
                }
            }
            Reference.UserContacts.AddRange(localObject);
            Reference.SaveChanges();



            //Image table for saving users Images
            string fileURL = "\\Images\\default.png";
            try
            {
                if (values.ImageURL.ContentLength > 0)
                {
                    string filename = Path.GetExtension(values.ImageURL.FileName);
                    string filePath = Path.Combine("\\Images\\", values.Firstname + filename);
                    string directoryPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), values.Firstname + filename);
                    values.ImageURL.SaveAs(directoryPath);
                    fileURL = filePath;
                }
            }
            catch(Exception exception)
            {
                
            }
            if (fileURL != string.Empty)
            {

                var localImage = new Image()
                {
                    UserId = userObject.UserId,
                    Title = userObject.Firstname,
                    ImageURL = fileURL
                };
                Reference.Images.Add(localImage);
                Reference.SaveChanges();
            }

            return values.Email;
        }




        /// <summary>
        /// Search user details and Send dashboard data to the user.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>UserDetails Object</returns>
        public DetailsModel ReadData(string email)
        {
            var dataObject = Reference.Users.Where(m => m.Email == email).SingleOrDefault();
            var image = Reference.Images.Where(m => m.UserId == dataObject.UserId).SingleOrDefault();
            var contactObject = Reference.UserContacts.Where(m => m.UserId == dataObject.UserId).Include(m => m.Contact).ToList();
            var detailsObject = new DetailsModel()
            {
                Firstname = dataObject.Firstname,
                Lastname = dataObject.Lastname,
                Email = dataObject.Email,
                Address = dataObject.Address,
                UserContact = contactObject,
                Image = image,
                UserId = dataObject.UserId
            };
            return detailsObject;
        }






        /// <summary>
        /// Login Method for Authenticate
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns>ID</returns>
        public int LogIn(LoginModel loginModel)
        {
            string localPassword = HashValue.HashValue.GetHashCode(loginModel.Password);
            var dataObject = Reference.Users.Where(m => m.Email.Equals(loginModel.Email) && m.Password.Equals(localPassword)).SingleOrDefault();
            return dataObject == null ? 0 : dataObject.UserId;
        }

    }
}